# blackjack-server
Blackjack Game Service

Fetches shuffled deck of cards and runs the round of Blackjack game with it. 

## Installation

```bash
$ yarn
```

## Running the app

```bash
# development
$ yarn start:dev

# production
$ yarn start:prod
```

## Test

```bash
# unit tests
$ yarn test

# e2e tests
$ yarn test:e2e

# test coverage
$ yarn test:cov
```
## License

UNLICENSED
