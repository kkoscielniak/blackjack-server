import { Deck, DeckType } from '../src/models/Deck';
import { Card, CardType, Suit } from '../src/models/Card';
import { Player } from '../src/models/Player';
import { NewGameRequestDto } from 'src/dtos/dtos';

export const PROTAGONIST_NAME = 'Catherine';
export const ANTAGONIST_NAME = 'Bob';

export const newGameRequest: NewGameRequestDto = {
  playerName: PROTAGONIST_NAME,
  turnBased: false,
};

export const newTurnBasedGameRequest: NewGameRequestDto = {
  playerName: PROTAGONIST_NAME,
  turnBased: true,
};

export const createTestCard = ({ suit, value }: Partial<CardType>): Card =>
  new Card({
    suit: suit || Suit.CLUBS,
    value,
  });

export const createTestDeck = ({ cards }: DeckType): Deck =>
  new Deck({ cards });

export const createTestPlayer = (initialCards: [Card, Card]) =>
  new Player({ initialCards, name: PROTAGONIST_NAME });

export type MockClass<T> = { [K in keyof T]: T[K] };
