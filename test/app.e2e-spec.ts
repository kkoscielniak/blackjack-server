import { Test, TestingModule } from '@nestjs/testing';
import { HttpStatus, INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/game/new (GET)', (done) => {
    request(app.getHttpServer())
      .get('/game/new')
      .expect(HttpStatus.OK)
      .end((req, res) => {
        expect(res.body).toMatchObject({
          winner: expect.any(String),
          players: expect.arrayContaining([
            expect.objectContaining({
              name: expect.any(String),
              points: expect.any(Number),
              cards: expect.arrayContaining([
                expect.stringMatching(/^[HDSC]([2-9JQKA]|10)$/),
              ]),
            }),
          ]),
        });

        done();
      });
  });

  it('/game/new with turnBased=true (GET)', (done) => {
    request(app.getHttpServer())
      .get('/game/new?turnBased=true')
      .expect(HttpStatus.OK)
      .end((req, res) => {
        expect(res.body).toMatchObject({
          winner: expect.any(String),
          players: expect.arrayContaining([
            expect.objectContaining({
              name: expect.any(String),
              points: expect.any(Number),
              cards: expect.arrayContaining([
                expect.stringMatching(/^[HDSC]([2-9JQKA]|10)$/),
              ]),
              firstTurn: {
                points: expect.any(Number),
                cards: expect.arrayContaining([
                  expect.stringMatching(/^[HDSC]([2-9JQKA]|10)$/),
                ]),
              },
              nextTurns: {
                points: expect.any(Number),
                cards: expect.arrayContaining([
                  expect.stringMatching(/^[HDSC]([2-9JQKA]|10)$/),
                ]),
              },
            }),
          ]),
        });

        done();
      });
  });
});
