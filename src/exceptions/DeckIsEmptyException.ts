export class DeckIsEmptyException extends Error {
  constructor() {
    super(`Cannot draw a card from an empty deck`);
  }
}
