export class ApiErrorException extends Error {
  constructor(message: string) {
    super(`The Deck API returned an error: ${message}`);
  }
}
