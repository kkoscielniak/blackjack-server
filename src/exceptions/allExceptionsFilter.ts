import {
  ArgumentsHost,
  BadGatewayException,
  Catch,
  HttpException,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { BaseExceptionFilter } from '@nestjs/core';
import { ApiErrorException } from './ApiErrorException';
import { DeckIsEmptyException } from './DeckIsEmptyException';

Catch();
export class AllExceptionsFilter extends BaseExceptionFilter {
  private logger = new Logger();

  catch(exception: ApiErrorException, host: ArgumentsHost) {
    this.logger.error(exception, exception.stack);

    const error = this.mapError(exception);
    super.catch(error, host);
  }

  private mapError(exception: Error): HttpException {
    if (exception instanceof ApiErrorException) {
      return new BadGatewayException(exception.message);
    }

    if (exception instanceof DeckIsEmptyException) {
      return new InternalServerErrorException(exception.message);
    }

    return new InternalServerErrorException(exception.constructor.name);
  }
}
