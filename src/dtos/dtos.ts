import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsNotEmpty, IsString } from 'class-validator';

export class NewGameRequestDto {
  @IsNotEmpty()
  @IsString()
  playerName!: string;

  @IsBoolean()
  turnBased?: boolean;
}

export class PlayerDto {
  @ApiProperty()
  name: string;

  @ApiProperty()
  points: number;

  @ApiProperty()
  cards: string[];
}

export class GameResultDto {
  @ApiProperty()
  winner: string;

  @ApiProperty({ type: [PlayerDto] })
  players: Array<PlayerDto>;
}
