const DEFAULT_PORT = 3001;
const DEFAULT_API_URL = 'https://pickatale-backend-case.herokuapp.com/shuffle';

type ConfigType = {
  port: number;
  apiUrl: string;
};

export default (): ConfigType => ({
  port: Number(process.env.PORT || DEFAULT_PORT),
  apiUrl: process.env.API_URL || DEFAULT_API_URL,
});
