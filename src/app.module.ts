import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { GameController } from './controllers/game/game.controller';
import { GameService } from './services/game/game.service';
import configuration from './config/configuration';
import { DeckRepository } from './repositories/deck/deck.repository';
import { HttpModule } from '@nestjs/axios';

const repositories = [DeckRepository];
const services = [GameService];

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration],
    }),
    HttpModule,
  ],
  controllers: [GameController],
  providers: [...services, ...repositories],
})
export class AppModule {}
