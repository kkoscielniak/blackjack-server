import { Card } from './Card';

const BLACKJACK_POINTS_SCORE = 21;

type PlayerType = {
  name: string;
  initialCards: Card[];
};

export class Player {
  readonly name: string;

  firstTurnCards: Card[] = [];
  firstTurnPoints = 0;

  nextTurnsCards: Card[] = [];
  nextTurnsPoints = 0;

  public get points(): number {
    return this.firstTurnPoints + this.nextTurnsPoints;
  }

  constructor({ initialCards, name }: PlayerType) {
    this.firstTurnCards.push(...initialCards);
    this.name = name;

    this.firstTurnPoints = this.calculatePointsOnHand(this.firstTurnCards);
  }

  public get cardsOnHand(): Card[] {
    return [...this.firstTurnCards, ...this.nextTurnsCards];
  }

  private calculatePointsOnHand(cards: Card[]): number {
    const points = cards.reduce((acc: number, card: Card) => {
      return acc + card.points;
    }, 0);

    return points;
  }

  addCardToHand(card: Card): void {
    this.nextTurnsCards.push(card);
    this.nextTurnsPoints = this.calculatePointsOnHand(this.nextTurnsCards);
  }

  hasBlackjack(): boolean {
    return this.points === BLACKJACK_POINTS_SCORE;
  }

  hasExcessivePoints(): boolean {
    return this.points > BLACKJACK_POINTS_SCORE;
  }
}
