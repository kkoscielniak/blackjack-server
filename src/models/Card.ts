export enum Suit {
  HEARTS = 'HEARTS',
  DIAMONDS = 'DIAMONDS',
  SPADES = 'SPADES',
  CLUBS = 'CLUBS',
}

enum SuitShorthand {
  HEARTS = 'H',
  DIAMONDS = 'D',
  SPADES = 'S',
  CLUBS = 'C',
}

export enum Value {
  TWO = '2',
  THREE = '3',
  FOUR = '4',
  FIVE = '5',
  SIX = '6',
  SEVEN = '7',
  EIGHT = '8',
  NINE = '9',
  TEN = '10',
  JACK = 'J',
  QUEEN = 'Q',
  KING = 'K',
  ACE = 'A',
}

export type CardType = {
  suit: Suit;
  value: Value;
};

export class Card {
  readonly suit: Suit;
  readonly value: Value;
  #points: number;

  public get points(): number {
    return this.#points;
  }

  constructor({ suit, value }: CardType) {
    this.suit = suit;
    this.value = value;

    this.#points = this.calculatePoints(value);
  }

  private calculatePoints(value: Value): number {
    switch (value) {
      case Value.ACE:
        return 11;
      case Value.KING:
      case Value.QUEEN:
      case Value.JACK:
        return 10;
      default:
        return Number(value);
    }
  }

  toString(): string {
    return `${SuitShorthand[this.suit]}${this.value}`;
  }
}
