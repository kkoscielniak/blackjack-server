import { NewGameRequestDto } from 'src/dtos/dtos';
import { Deck } from './Deck';
import { Player } from './Player';

type GameConstructorType = {
  deck: Deck;
  newGameRequest?: NewGameRequestDto;
};

enum GameKind {
  NORMAL,
  TURN_BASED,
}

export type ResultType = {
  winner: string;
  players: Array<{
    name: string;
    points: number;
    cards: string[];
    firstTurn?: {
      cards: string[];
      points: number;
    };
    nextTurns?: {
      cards: string[];
      points: number;
    };
  }>;
};

const MAXIMAL_POINTS_FOR_PROTAGONIST = 17;
export const TIE_STRING = "It's a tie! 😅";

export class Game {
  #kind: GameKind;
  #deck: Deck;
  #protagonist: Player;
  #antagonist: Player; // Bob

  constructor({ deck, newGameRequest }: GameConstructorType) {
    this.#deck = deck;

    this.#protagonist = new Player({
      initialCards: [this.#deck.drawCard(), this.#deck.drawCard()],
      name: newGameRequest?.playerName || 'Alice',
    });

    this.#antagonist = new Player({
      initialCards: [this.#deck.drawCard(), this.#deck.drawCard()],
      name: 'Bob',
    });

    this.#kind = newGameRequest?.turnBased
      ? GameKind.TURN_BASED
      : GameKind.NORMAL;
  }

  private checkForWinners(): Player | null {
    if (this.#protagonist.hasBlackjack()) {
      return this.#protagonist;
    }

    if (this.#antagonist.hasBlackjack()) {
      return this.#antagonist;
    }

    return null;
  }

  private checkForTie(): boolean {
    if (
      this.#protagonist.cardsOnHand.length !== 2 ||
      this.#antagonist.cardsOnHand.length !== 2
    ) {
      return false;
    }

    const bothPlayersWon =
      this.#protagonist.hasBlackjack() && this.#antagonist.hasBlackjack();

    const bothPlayersLost =
      this.#protagonist.hasExcessivePoints() &&
      this.#antagonist.hasExcessivePoints();

    return bothPlayersWon || bothPlayersLost;
  }

  private drawCards(player: Player, maximalNumberOfPoints: number): void {
    while (player.points < maximalNumberOfPoints) {
      player.addCardToHand(this.#deck.drawCard());
    }
  }

  private createResultResponse(winner: Player | null): ResultType {
    const playerObjectToResponseMapper = ({
      name,
      points,
      cardsOnHand,
      firstTurnCards,
      firstTurnPoints,
      nextTurnsCards,
      nextTurnsPoints,
    }) => {
      const response = {
        name,
        points,
        cards: cardsOnHand.map((card) => card.toString()),
      };

      return this.#kind === GameKind.TURN_BASED
        ? {
            ...response,
            firstTurn: {
              cards: firstTurnCards.map((card) => card.toString()),
              points: firstTurnPoints,
            },
            nextTurns: {
              cards: nextTurnsCards.map((card) => card.toString()),
              points: nextTurnsPoints,
            },
          }
        : response;
    };

    return {
      winner: winner?.name || TIE_STRING,
      players: [this.#protagonist, this.#antagonist].map(
        playerObjectToResponseMapper,
      ),
    };
  }

  play(): ResultType {
    const isTie = this.checkForTie();

    if (isTie) {
      return this.createResultResponse(null);
    }

    const winner = this.checkForWinners();

    if (winner) {
      return this.createResultResponse(winner);
    }

    if (this.#antagonist.hasExcessivePoints()) {
      return this.createResultResponse(this.#protagonist);
    }

    this.drawCards(this.#protagonist, MAXIMAL_POINTS_FOR_PROTAGONIST);

    if (this.#protagonist.hasExcessivePoints()) {
      return this.createResultResponse(this.#antagonist);
    }

    this.drawCards(this.#antagonist, this.#protagonist.points + 1);

    if (this.#antagonist.hasExcessivePoints()) {
      return this.createResultResponse(this.#protagonist);
    }

    return this.createResultResponse(this.#antagonist);
  }
}
