import { createTestCard } from '../../../test/helpers';
import { Card, Suit, Value } from '../Card';

describe('Card model', () => {
  it('should create a Card', () => {
    expect(
      () => new Card({ suit: Suit.CLUBS, value: Value.ACE }),
    ).not.toThrow();
  });

  it('should stringify card properly', () => {
    const card = createTestCard({ value: Value.ACE });
    const stringifiedCard = card.toString();

    expect(stringifiedCard).toEqual('CA');
  });

  it('should calculate points properly', () => {
    const cards = [
      createTestCard({ value: Value.ACE }),
      createTestCard({ value: Value.KING }),
      createTestCard({ value: Value.QUEEN }),
      createTestCard({ value: Value.JACK }),
      createTestCard({ value: Value.TEN }),
      createTestCard({ value: Value.NINE }),
    ];

    expect(cards.map((card) => card.points)).toStrictEqual([
      11, 10, 10, 10, 10, 9,
    ]);
  });
});
