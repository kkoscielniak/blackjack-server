import { DeckIsEmptyException } from '../../exceptions/DeckIsEmptyException';
import { createTestCard, createTestDeck } from '../../../test/helpers';
import { Value } from '../Card';
import { Deck } from '../Deck';

describe('Deck model', () => {
  const cards = [
    createTestCard({ value: Value.ACE }),
    createTestCard({ value: Value.KING }),
  ];

  it('should create a Deck', () => {
    expect(() => new Deck({ cards })).not.toThrow();
  });

  it('should draw from Deck', () => {
    const deck = createTestDeck({ cards });
    const card = deck.drawCard();
    expect(card).toEqual(cards[0]);
  });

  it('should throw an error if trying to draw from an empty Deck', () => {
    const cards = [
      createTestCard({ value: Value.ACE }),
      createTestCard({ value: Value.KING }),
    ];

    const deck = createTestDeck({ cards });

    deck.drawCard();
    deck.drawCard();
    // all cards have been drawn already

    expect(() => deck.drawCard()).toThrow(DeckIsEmptyException);
  });
});
