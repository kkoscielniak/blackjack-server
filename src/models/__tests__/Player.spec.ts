import {
  createTestCard,
  createTestPlayer,
  PROTAGONIST_NAME,
} from '../../../test/helpers';
import { Value } from '../Card';
import { Player } from '../Player';

describe('Player model', () => {
  let initialCards, drawnCard;
  beforeEach(() => {
    initialCards = [
      createTestCard({ value: Value.ACE }),
      createTestCard({ value: Value.KING }),
    ];

    drawnCard = createTestCard({ value: Value.TWO });
  });

  it('should create a Player', () => {
    const initialCards = [
      createTestCard({ value: Value.ACE }),
      createTestCard({ value: Value.KING }),
    ];

    expect(
      () => new Player({ initialCards, name: PROTAGONIST_NAME }),
    ).not.toThrow();
  });

  it('should calculate points properly', () => {
    const player = createTestPlayer(initialCards);

    expect(player.points).toBe(21);
  });

  it('should add drawn card to hand', () => {
    const player = createTestPlayer(initialCards);
    player.addCardToHand(drawnCard);

    expect(player.cardsOnHand).toContain(drawnCard);
  });

  describe('hasBlackjack()', () => {
    it('should return true for initial hand worth of 21 points', () => {
      const player = createTestPlayer(initialCards);

      expect(player.hasBlackjack()).toBeTruthy();
    });

    it('should return false for hand worth of more than 21 points', () => {
      const player = createTestPlayer(initialCards);
      player.addCardToHand(drawnCard);

      expect(player.hasBlackjack()).toBeFalsy();
    });
  });

  describe('hasExcessivePoints()', () => {
    it('should return true for hand worth of more than 21 points', () => {
      const player = createTestPlayer(initialCards);
      player.addCardToHand(drawnCard);

      expect(player.hasExcessivePoints()).toBeTruthy();
    });
  });
});
