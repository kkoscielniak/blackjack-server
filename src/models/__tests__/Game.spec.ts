import {
  createTestCard,
  createTestDeck,
  newGameRequest,
  newTurnBasedGameRequest,
  PROTAGONIST_NAME,
  ANTAGONIST_NAME,
} from '../../../test/helpers';
import { Suit, Value } from '../Card';
import { DeckType } from '../Deck';
import { Game, ResultType, TIE_STRING } from '../Game';

const playGame = ({ cards }: DeckType): ResultType => {
  const deck = createTestDeck({ cards });
  const game = new Game({
    newGameRequest,
    deck,
  });

  return game.play();
};

describe('Game model', () => {
  it('should create a Game', () => {
    const cards = [
      createTestCard({ value: Value.ACE }),
      createTestCard({ value: Value.KING }),
      createTestCard({ value: Value.QUEEN }),
      createTestCard({ value: Value.ACE }),
    ];

    const deck = createTestDeck({ cards });

    expect(
      () =>
        new Game({
          newGameRequest: { playerName: PROTAGONIST_NAME, turnBased: false },
          deck,
        }),
    ).not.toThrow();
  });

  it('should finish a Game if the protagonist has blackjack', () => {
    const cards = [
      createTestCard({ value: Value.ACE }),
      createTestCard({ value: Value.TEN }),
      // protagonist: 21 points, immediate win

      createTestCard({ value: Value.TWO }),
      createTestCard({ value: Value.FOUR }),
      // angatonist: 6 points
    ];

    const result = playGame({ cards });
    expect(result.winner).toEqual(PROTAGONIST_NAME);
  });

  it('should finish a Game if the antagonist has blackjack', () => {
    const cards = [
      createTestCard({ value: Value.TWO }),
      createTestCard({ value: Value.FOUR }),
      // protagonist: 6 points

      createTestCard({ value: Value.ACE }),
      createTestCard({ value: Value.TEN }),
      // antagonist: 21 points, immediate win
    ];

    const result = playGame({ cards });
    expect(result.winner).toEqual(ANTAGONIST_NAME);
  });

  it('should go through the "game loop" until one of the players exceedes maximal number of points', () => {
    const cards = [
      createTestCard({ value: Value.FIVE }),
      createTestCard({ value: Value.FOUR }),
      // protagonist: 9 points

      createTestCard({ value: Value.TWO }),
      createTestCard({ value: Value.THREE }),
      // antagonist: 5 points

      createTestCard({ value: Value.SIX }),
      createTestCard({ value: Value.KING }),
      // protagonist: 9 + 16 = 25, lose
    ];

    const result = playGame({ cards });
    expect(result.winner).toBe(ANTAGONIST_NAME);
  });

  it('should check who won if no one gets 21 points', () => {
    const cards = [
      createTestCard({ value: Value.FOUR }),
      createTestCard({ value: Value.FIVE }),
      // protagonist: 9 points

      createTestCard({ value: Value.TWO }),
      createTestCard({ value: Value.THREE }),
      // antagonist: 5 points

      createTestCard({ value: Value.FOUR }),
      createTestCard({ value: Value.SIX }),
      // protagonist: 9 + 10 = 19, stop drawing

      createTestCard({ value: Value.TEN }),
      createTestCard({ value: Value.FIVE }),
      // antagonist: 5 + 15 = 20, win
    ];

    const result = playGame({ cards });

    expect(result.winner).toEqual(ANTAGONIST_NAME);
  });

  it('should make protagonist a winner if the antagonist exceedes 21 points', () => {
    const cards = [
      createTestCard({ value: Value.FIVE }),
      createTestCard({ value: Value.FOUR }),
      // protagonist: 9 points

      createTestCard({ value: Value.TWO }),
      createTestCard({ value: Value.THREE }),
      // antagonist: 5 points

      createTestCard({ value: Value.SIX }),
      createTestCard({ value: Value.FOUR }),
      // protagonist: 9 + 10 = 19 points, finish drawing

      createTestCard({ value: Value.TEN }),
      createTestCard({ value: Value.ACE }),
      // antagonist: 5 + 21 = 26 points, lose
    ];

    const result = playGame({ cards });

    expect(result.winner).toEqual(PROTAGONIST_NAME);
  });

  it('should return TIE_STRING if both players have blackjack', () => {
    const cards = [
      createTestCard({ value: Value.ACE }),
      createTestCard({ value: Value.TEN }),
      // protagonist: 21 points

      createTestCard({ value: Value.QUEEN }),
      createTestCard({ value: Value.ACE }),
      // antagonist: 21 points
    ];

    const result = playGame({ cards });

    expect(result.winner).toEqual(TIE_STRING);
  });

  it('should return TIE_STRING if both players lost by exceeding points', () => {
    const cards = [
      createTestCard({ value: Value.ACE }),
      createTestCard({ value: Value.ACE }),
      // protagonist: 22 points

      createTestCard({ value: Value.ACE }),
      createTestCard({ value: Value.ACE }),
      // antagonist: 22 points
    ];

    const result = playGame({ cards });

    expect(result.winner).toEqual(TIE_STRING);
  });

  it('should stringify cards properly in response', () => {
    const cards = [
      createTestCard({ suit: Suit.SPADES, value: Value.ACE }),
      createTestCard({ suit: Suit.DIAMONDS, value: Value.TEN }),
      // protagonist: 21 points, immediate win

      createTestCard({ value: Value.TWO }),
      createTestCard({ value: Value.FOUR }),
      // angatonist: 6 points
    ];

    const result = playGame({ cards });
    expect(result.players[0].cards).toEqual(['SA', 'D10']);
  });

  it('should append additional result data to response for turn based game', () => {
    const cards = [
      createTestCard({ value: Value.FIVE }),
      createTestCard({ value: Value.FOUR }),
      // protagonist: 9 points

      createTestCard({ value: Value.TWO }),
      createTestCard({ value: Value.THREE }),
      // antagonist: 5 points

      createTestCard({ value: Value.SIX }),
      createTestCard({ value: Value.FOUR }),
      // protagonist: 9 + 10 = 19 points, finish drawing

      createTestCard({ value: Value.TEN }),
      createTestCard({ value: Value.ACE }),
      // antagonist: 5 + 21 = 26 points, lose
    ];

    const deck = createTestDeck({ cards });
    const game = new Game({
      newGameRequest: newTurnBasedGameRequest,
      deck,
    });

    const result = game.play();

    expect(result.players[0]).toEqual(
      expect.objectContaining({
        firstTurn: expect.objectContaining({
          cards: expect.arrayContaining(['C5', 'C4']),
          points: 9,
        }),
        nextTurns: expect.objectContaining({
          cards: expect.arrayContaining(['C6', 'C4']),
          points: 10,
        }),
      }),
    );

    expect(result.players[1]).toEqual(
      expect.objectContaining({
        firstTurn: expect.objectContaining({
          cards: expect.arrayContaining(['C2', 'C3']),
          points: 5,
        }),
        nextTurns: expect.objectContaining({
          cards: expect.arrayContaining(['C10', 'CA']),
          points: 21,
        }),
      }),
    );
  });
});
