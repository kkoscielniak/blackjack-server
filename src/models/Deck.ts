import { DeckIsEmptyException } from '../exceptions/DeckIsEmptyException';
import { Card } from './Card';

export type DeckType = {
  cards: Array<Card>;
};

export class Deck {
  #cards: Array<Card>;

  constructor({ cards }: DeckType) {
    this.#cards = cards;
  }

  drawCard(): Card {
    if (!this.#cards.length) {
      throw new DeckIsEmptyException();
    }

    const [cardOnTop, ...restOfDeck] = this.#cards;
    this.#cards = restOfDeck;
    return cardOnTop;
  }

  getAllCards() {
    return this.#cards;
  }
}
