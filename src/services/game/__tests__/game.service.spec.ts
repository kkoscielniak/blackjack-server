import { ConfigModule } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import { Suit, Value } from '../../../models/Card';
import {
  ANTAGONIST_NAME,
  createTestCard,
  createTestDeck,
  newGameRequest,
  PROTAGONIST_NAME,
} from '../../../../test/helpers';
import configuration from '../../../config/configuration';
import { DeckRepository } from '../../../repositories/deck/deck.repository';
import { MockDeckRepository } from '../../../repositories/deck/__tests__/deck.repository.mock';
import { GameService } from '../game.service';
import { TIE_STRING } from '../../../models/Game';

describe('Game service', () => {
  let gameService: GameService;
  let deckRepo: MockDeckRepository;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [ConfigModule.forRoot({ load: [configuration] })],
      providers: [GameService, DeckRepository],
    })
      .overrideProvider(DeckRepository)
      .useClass(MockDeckRepository)
      .compile();

    gameService = module.get(GameService);
    deckRepo = module.get(DeckRepository);
  });

  describe('startNewGame(): immediate wins', () => {
    it('should handle protagonist win in the first round', async () => {
      const cards = [
        createTestCard({ value: Value.ACE }),
        createTestCard({ value: Value.KING }),

        createTestCard({ value: Value.QUEEN }),
        createTestCard({ value: Value.JACK }),
      ];
      const deck = createTestDeck({ cards });
      deckRepo.getShuffledDeck.mockResolvedValueOnce(deck);

      const result = await gameService.startNewGame(newGameRequest);

      expect(result.winner).toEqual(PROTAGONIST_NAME);
      expect(result.players[1].cards.length).toEqual(2);
    });

    it('should handle antagonist win in the first round', async () => {
      const cards = [
        createTestCard({ value: Value.QUEEN }),
        createTestCard({ value: Value.JACK }),

        createTestCard({ value: Value.ACE }),
        createTestCard({ value: Value.KING }),
      ];
      const deck = createTestDeck({ cards });
      deckRepo.getShuffledDeck.mockResolvedValueOnce(deck);

      const result = await gameService.startNewGame(newGameRequest);

      expect(result.winner).toEqual(ANTAGONIST_NAME);
      expect(result.players[1].cards.length).toEqual(2);
    });
  });

  describe('startNewGame(): immediate loses', () => {
    it('should handle protagonist loss in the first round', async () => {
      const cards = [
        createTestCard({ value: Value.ACE }),
        createTestCard({ suit: Suit.DIAMONDS, value: Value.ACE }),

        createTestCard({ value: Value.QUEEN }),
        createTestCard({ value: Value.JACK }),
      ];
      const deck = createTestDeck({ cards });
      deckRepo.getShuffledDeck.mockResolvedValueOnce(deck);

      const result = await gameService.startNewGame(newGameRequest);

      expect(result.winner).toEqual(ANTAGONIST_NAME);
      expect(result.players[1].cards.length).toEqual(2);
      expect(result.players[0].points).toEqual(22);
    });

    it('should handle antagonist loss in the first round', async () => {
      const cards = [
        createTestCard({ value: Value.QUEEN }),
        createTestCard({ value: Value.JACK }),

        createTestCard({ value: Value.ACE }),
        createTestCard({ suit: Suit.DIAMONDS, value: Value.ACE }),
      ];
      const deck = createTestDeck({ cards });
      deckRepo.getShuffledDeck.mockResolvedValueOnce(deck);

      const result = await gameService.startNewGame(newGameRequest);

      expect(result.winner).toEqual(PROTAGONIST_NAME);
      expect(result.players[1].cards.length).toEqual(2);
      expect(result.players[1].points).toEqual(22);
    });
  });

  describe('startNewGame(): wins by higher value', () => {
    it("should handle antagonist's win by higher value", async () => {
      const cards = [
        createTestCard({ value: Value.NINE }),
        createTestCard({ value: Value.TEN }),

        createTestCard({ value: Value.FOUR }),
        createTestCard({ value: Value.TWO }),

        createTestCard({ suit: Suit.DIAMONDS, value: Value.FOUR }),

        createTestCard({ value: Value.SIX }),
        createTestCard({ value: Value.FIVE }),
      ];
      const deck = createTestDeck({ cards });
      deckRepo.getShuffledDeck.mockResolvedValueOnce(deck);

      const result = await gameService.startNewGame(newGameRequest);

      expect(result.winner).toEqual(ANTAGONIST_NAME);
      expect(result.players[0].points).toEqual(19);
      expect(result.players[1].points).toEqual(21);
    });
  });

  describe('startNewGame(): loses by exceeding maximal points number', () => {
    it("should handle protagonist's loss", async () => {
      const cards = [
        createTestCard({ value: Value.TEN }),
        createTestCard({ value: Value.SIX }),

        createTestCard({ value: Value.TWO }),
        createTestCard({ value: Value.THREE }),

        createTestCard({ value: Value.TEN }),
      ];
      const deck = createTestDeck({ cards });
      deckRepo.getShuffledDeck.mockResolvedValueOnce(deck);
      const result = await gameService.startNewGame(newGameRequest);

      expect(result.winner).toEqual(ANTAGONIST_NAME);
      expect(result.players[0].points).toEqual(26);
    });

    it("should handle antagonist's loss", async () => {
      const cards = [
        createTestCard({ value: Value.TEN }),
        createTestCard({ value: Value.FOUR }),

        createTestCard({ value: Value.TEN }),
        createTestCard({ value: Value.THREE }),

        createTestCard({ value: Value.THREE }),

        createTestCard({ value: Value.TEN }),
      ];
      const deck = createTestDeck({ cards });
      deckRepo.getShuffledDeck.mockResolvedValueOnce(deck);
      const result = await gameService.startNewGame(newGameRequest);

      expect(result.winner).toEqual(PROTAGONIST_NAME);
      expect(result.players[1].points).toEqual(23);
    });
  });

  describe('startNewGame(): ties', () => {
    it('should handle a winning tie', async () => {
      const cards = [
        createTestCard({ value: Value.ACE }),
        createTestCard({ value: Value.KING }),

        createTestCard({ suit: Suit.DIAMONDS, value: Value.ACE }),
        createTestCard({ suit: Suit.DIAMONDS, value: Value.KING }),
      ];
      const deck = createTestDeck({ cards });
      deckRepo.getShuffledDeck.mockResolvedValueOnce(deck);

      const result = await gameService.startNewGame(newGameRequest);

      expect(result.winner).toEqual(TIE_STRING);
      expect(result.players[1].cards.length).toEqual(2);
    });

    it('should handle a losing tie', async () => {
      const cards = [
        createTestCard({ value: Value.ACE }),
        createTestCard({ suit: Suit.HEARTS, value: Value.ACE }),

        createTestCard({ suit: Suit.DIAMONDS, value: Value.ACE }),
        createTestCard({ suit: Suit.SPADES, value: Value.ACE }),
      ];
      const deck = createTestDeck({ cards });
      deckRepo.getShuffledDeck.mockResolvedValueOnce(deck);

      const result = await gameService.startNewGame(newGameRequest);

      expect(result.winner).toEqual(TIE_STRING);
      expect(result.players[1].cards.length).toEqual(2);
    });
  });
});
