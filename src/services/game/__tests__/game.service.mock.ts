import { Injectable } from '@nestjs/common';
import { MockClass } from '../../../../test/helpers';
import { GameService } from '../game.service';

Injectable();
export class MockGameService implements MockClass<GameService> {
  startNewGame = jest.fn();
}
