import { Injectable } from '@nestjs/common';
import { NewGameRequestDto } from 'src/dtos/dtos';
import { Game, ResultType } from '../../models/Game';
import { DeckRepository } from '../../repositories/deck/deck.repository';

@Injectable()
export class GameService {
  constructor(private deckRepo: DeckRepository) {}

  async startNewGame(newGameRequest: NewGameRequestDto): Promise<ResultType> {
    const deck = await this.deckRepo.getShuffledDeck();

    const game = new Game({ deck, newGameRequest });
    return game.play();
  }
}
