import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { firstValueFrom } from 'rxjs';
import { ApiErrorException } from '../../exceptions/ApiErrorException';
import { Card } from '../../models/Card';
import { Deck } from '../../models/Deck';

@Injectable()
export class DeckRepository {
  constructor(
    private configService: ConfigService,
    private httpClient: HttpService,
  ) {}

  async getShuffledDeck(): Promise<Deck> {
    const API_URL = this.configService.get<string>('apiUrl');

    try {
      const response = await this.httpClient.get(API_URL);
      const { data: fetchedCards } = await firstValueFrom(response);

      const mappedCards = fetchedCards.map((fetchedCard) => {
        return new Card(fetchedCard);
      });

      const deck = new Deck({ cards: mappedCards });
      return deck;
    } catch (e) {
      throw new ApiErrorException(e.message);
    }
  }
}
