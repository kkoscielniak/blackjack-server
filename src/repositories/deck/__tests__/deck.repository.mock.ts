import { Injectable } from '@nestjs/common';
import { MockClass } from '../../../../test/helpers';
import { DeckRepository } from '../deck.repository';

@Injectable()
export class MockDeckRepository implements MockClass<DeckRepository> {
  getShuffledDeck = jest.fn();
}
