import { Controller, Get, HttpCode, HttpStatus, Query } from '@nestjs/common';
import {
  ApiCreatedResponse,
  ApiOperation,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { GameResultDto } from '../../dtos/dtos';
import { GameService } from '../../services/game/game.service';

@Controller('game')
export class GameController {
  constructor(private game: GameService) {}

  @Get('new')
  @HttpCode(HttpStatus.OK)
  @ApiTags('game')
  @ApiOperation({ summary: 'Start new game and get its result' })
  @ApiCreatedResponse({ type: GameResultDto })
  @ApiQuery({
    name: 'name',
    required: false,
    description: 'Protagonist name',
    type: 'string',
    example: 'Alice',
  })
  @ApiQuery({
    name: 'turnBased',
    required: false,
    description: 'Split drawn cards to firstTurn and nextTurns objects',
    type: 'boolean',
    example: 'false',
  })
  newGame(@Query() query): Promise<GameResultDto> {
    return this.game.startNewGame({
      playerName: query.name,
      turnBased: query.turnBased === 'true',
    });
  }
}
