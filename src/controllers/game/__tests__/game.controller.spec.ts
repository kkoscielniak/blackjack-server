import { HttpStatus } from '@nestjs/common';
import { NestApplication } from '@nestjs/core';
import { Test } from '@nestjs/testing';
import * as supertest from 'supertest';
import { PROTAGONIST_NAME } from '../../../../test/helpers';
import { GameService } from '../../../services/game/game.service';
import { MockGameService } from '../../../services/game/__tests__/game.service.mock';
import { GameController } from '../game.controller';

describe('GamesController HTTP', () => {
  let gameService: MockGameService;
  let app: NestApplication;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [],
      controllers: [GameController],
      providers: [GameService],
    })
      .overrideProvider(GameService)
      .useClass(MockGameService)
      .compile();

    gameService = module.get(GameService);
    app = module.createNestApplication();
    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  it('GET /game/new', () => {
    gameService.startNewGame.mockResolvedValue({ winner: PROTAGONIST_NAME });

    return supertest(app.getHttpServer())
      .get('/game/new')
      .expect(HttpStatus.OK);
  });

  const QUERY_PARAM_NAME = 'Dave';
  it(`GET /game/new?name=${QUERY_PARAM_NAME}`, () => {
    gameService.startNewGame.mockResolvedValue({ winner: QUERY_PARAM_NAME });

    return supertest(app.getHttpServer())
      .get('/game/new')
      .query({ name: QUERY_PARAM_NAME })
      .expect(HttpStatus.OK);
  });
});
